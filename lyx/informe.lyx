#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass scrbook
\begin_preamble
% Tipo de Letra (Times New Roman)
\usepackage{microtype}
\usepackage{txfonts}

% Verbatim Styles
\usepackage{verbatim} 	% adds environment for commenting out blocks of text
\usepackage{fancyvrb}	% fancy verbatim

% Cambiar los Títulos (sólo en español (Chile))
\addto{\captionsspanish}{\renewcommand{\contentsname}{Índice de Contenidos}}
\addto{\captionsspanish}{\renewcommand{\listfigurename}{Índice de Figuras}}
\addto{\captionsspanish}{\renewcommand{\listtablename}{Índice de Tablas}}
\addto{\captionsspanish}{\renewcommand{\tablename}{Tabla}}
\addto{\captionsspanish}{\renewcommand{\appendixname}{Anexo}}

% Figures and Captions
\usepackage{subfigure} 	% include more than one captioned figure/table in a single float
\usepackage[font=small,labelfont=bf]{caption}   % Caption Fonts and Weight

%%% APPEARANCE
\usepackage{booktabs} 	% for much better looking tables
\usepackage{tabularx}	% for much better looking tables (might conflict with previous)
\usepackage{multirow}	% for multirow tables
\usepackage{array} 	% for better arrays (eg matrices) in maths
\usepackage{float}	% for floating figures
\usepackage{textcomp}
\usepackage{endnotes}
\usepackage{paralist}	% very flexible & customisable lists (eg. enumerate/itemize, etc.)

\usepackage{setspace}	% linespacing
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
%\usepackage{showframe} 	% Show frames (formatting aid)
%\usepackage[section]{placeins}

%---------------------------------------------------------------------------
%%% CHAPTER HEADINGS
\setkomafont{disposition}{\bfseries}
% Alternative:
% Options: Sonny, Lenny, Glenn, Conny, Rejne, Bjarne, Bjornstrup
%\usepackage[Bjornstrup]{fncychap}
%---------------------------------------------------------------------------

%---------------------------------------------------------------------------
%%% FIGURES, GRAPHICS AND COLORS
\usepackage{wrapfig}			% flooating Figures and Tables
\usepackage{xcolor}			% custom colors
\definecolor{gray80}{gray}{0.80}
\definecolor{grey}{RGB} {132,132,132}
\usepackage{graphicx}		% graphics library
\usepackage{subfigure} 	% include more than one captioned figure/table in a single float
%---------------------------------------------------------------------------

%---------------------------------------------------------------------------
%%% HEADERS AND FOOTERS
\usepackage[markuppercase,ilines]{scrpage2}
\automark[section]{chapter}	% [leftmark]{rightmark}
%\clearscrheadfoot
\pagestyle{scrheadings}
\renewcommand*{\chapterpagestyle}{scrheadings}
\setkomafont{pagehead}{\color{grey}\scriptsize}
\setkomafont{pagefoot}{\color{grey}\normalsize}
\setkomafont{pagenumber}{\color{grey}\bfseries}
\setheadsepline{.1pt}[\color{grey}]
\setfootsepline{.1pt}[\color{grey}]
\chead{}
\ohead[]{\hfill\rightmark}
\ihead[]{\leftmark}
\cfoot[]{}
\ifoot[{\includegraphics[height=2.5mm]{figures/logousm.jpg} {\small Departamento de Industrias, Universidad Técnica Federico Santa María} }]{\includegraphics[height=2.5mm]{figures/logousm.jpg} {\small Departamento de Industrias, Universidad Técnica Federico Santa María}}
\ofoot[\pagemark]{\pagemark}
%---------------------------------------------------------------------------


%%% WATERMARK
% \usepackage{graphicx} %Required, if not loaded already!
\usepackage{type1cm}
\usepackage{eso-pic}
\usepackage{color}
\makeatletter
\AddToShipoutPicture{%
            \setlength{\@tempdimb}{.36\paperwidth}%
            \setlength{\@tempdimc}{.60\paperheight}%
            \setlength{\unitlength}{1pt}%
            \put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
            %\makebox(0,0){\rotatebox{45}{\textcolor[gray]{0.9}%
            %{\fontsize{6cm}{6cm}\selectfont{DRAFT}}}}%
            \includegraphics[width=.4\textwidth]{figures/logousm_watermark.jpg}
            }%
}

\usepackage[longnamesfirst]{natbib}


\makeatother

%---------------------------------------------------------------------------
%%% TYPESETTING AIDS
\usepackage{lipsum}			% automated text generation
\usepackage{blindtext}	% automated text generation
%\usepackage{showframe} 	% Show frames (formatting aid)
%---------------------------------------------------------------------------
\end_preamble
\options oneside
\use_default_options true
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing single
\use_hyperref true
\pdf_title "Plantilla para Prácticas USM"
\pdf_author "JCR"
\pdf_subject "Prácticas"
\pdf_keywords "Prácticas"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks true
\pdf_pdfborder true
\pdf_colorlinks true
\pdf_backref false
\pdf_pdfusetitle true
\pdf_quoted_options "colorlinks=true,linkcolor=blue,urlcolor=blue,citecolor=red"
\papersize letterpaper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 0
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 30mm
\topmargin 30mm
\rightmargin 30mm
\bottommargin 30mm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle empty
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
%%%%%%%%%%%%%%%%%%%%
\end_layout

\begin_layout Plain Layout
% Plantilla para Prácticas, UTFSM, Chile
\end_layout

\begin_layout Plain Layout
% ==============================
\end_layout

\begin_layout Plain Layout
%
\end_layout

\begin_layout Plain Layout
% VERSIÓN: 1.4.1
\end_layout

\begin_layout Plain Layout
%
\end_layout

\begin_layout Plain Layout
% AUTOR: Jaime C.
 Rubin-de-Celis <jaime@rubin-de-celis.com> 
\end_layout

\begin_layout Plain Layout
% 
\end_layout

\begin_layout Plain Layout
% FECHA: $Date: 2015-07-14 00:04:12 -0400$ 
\end_layout

\begin_layout Plain Layout
% 
\end_layout

\begin_layout Plain Layout
% LICENCIA: 
\end_layout

\begin_layout Plain Layout
% Copyright (c) 2012-2015, Jaime C.
 Rubin-de-Celis 
\end_layout

\begin_layout Plain Layout
% Licensed under the Educational Community License version 1.0
\end_layout

\begin_layout Plain Layout
% (Se excluyen las imágenes propiedad de la UTFSM y el Departamento de Industria
s) 
\end_layout

\begin_layout Plain Layout
%
\end_layout

\begin_layout Plain Layout
% NOTA: PDF fue compilado usando LyX 2.1.4 & MacTex-2015 (Apple OS X 10.10.4)
\end_layout

\begin_layout Plain Layout
% NOTA: PDF fue compilado usando LyX 2.2.0 & TexLive-8.20140525_r34255.f22 (Fedora
 22)
\end_layout

\begin_layout Plain Layout
% NOTA: En sistemas Windows, es posible que deba eliminar del preámbulo
 el paquete : microtype.
\end_layout

\begin_layout Plain Layout
% (Resultados podrían variar dependiendo de la plataforma.
 Revisar Notas del Preámbulo)
\end_layout

\begin_layout Plain Layout
% %%%%%%%%%%%%%%%%%%%%
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
frontmatter
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "portada.lyx"

\end_inset


\begin_inset CommandInset citation
LatexCommand nocite
key "lyx"

\end_inset


\begin_inset CommandInset citation
LatexCommand nocite
key "latex:whatis"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{spacing}{1}
\end_layout

\end_inset


\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{spacing}
\end_layout

\end_inset


\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
mainmatter
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap1.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap2.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap3.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap4.lyx"

\end_inset

 
\begin_inset Note Note
status open

\begin_layout Plain Layout
Sólo para Práctica Profesional (puede eliminar esta línea para Práctica
 Industrial).
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "semanal.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Bibliografía: Editar el archivo internship.bib con un editor de texto o con
 un editor bibliográfico.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{spacing}{1}
\end_layout

\end_inset


\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "internship"
options "usm"

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{spacing}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\start_of_appendix
\begin_inset CommandInset include
LatexCommand input
filename "appendix1.lyx"

\end_inset


\end_layout

\end_body
\end_document
